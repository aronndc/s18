// Function able to receive data without the use of global variables or prompt()

//"name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked.
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Louies");
printName("Aronn");

// Data passed into the function through function invocation is called arguments.
// The argument is then stored within a container called a parameter.
function printMyAge(age){
	console.log("I am " + age + " years old.")
};

printMyAge(25);
printMyAge();

// check the visibility reusably using a function with arguments and paremeters.
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};


checkDivisibilityBy8(64);
checkDivisibilityBy8(27);
/*
	Mini activity

	1. Create a function which is able to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	2. Create a function which is capable to receive a number as an argument:
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
*/


function faveSuperhero(hero){
	console.log("My favorite Superhero is " + hero);
};

faveSuperhero("Iron Man");

function evenNumber(num){
	let remainder = num % 2;
	let evenNumber = remainder === 0;
	console.log("Is " + num + " an even number?");
	console.log(evenNumber);
};

evenNumber(50);
evenNumber(51);
evenNumber(150);

// Multiple Arguments can also be passed into a function; multiple parameters can containt our arguments

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial + ' ' + lastName);
};

printFullName('Juan', 'Crisostomo', 'Ibarra');
printFullName('Ibarra', 'Juan', 'Crisostomo');

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an error or changes the behavior of the function.
*/

printFullName('Stephen', 'Wardell');
printFullName('Stephen', 'Wardell', 'Curry', 'Thomas');
printFullName('Stephen', 'Wardell', 'Curry');
printFullName('Stephen', '', 'Curry');

// Use variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName)

/* Mini - Activity
	Create a function which will be able to receive 5 arguments
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when the function is invoked.

*/

function topFaveSongs(first, second, third, fourth, fifth){
	console.log('My top 5 songs are:');
	console.log(first + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth);

	return first + second + third + fourth + fifth;
//	return "Hello World!";
};

topFaveSongs("While I'm Waiting,", 'Who am I,', 'Nothing is Impossible,', 'Voice of Truth,', 'One Way.' )

// Return Statement
// Currently or so far, our function are able to display data in our console
// However, our fufnction cannot yet return values. Function are able to return values which can be save into a variable using the return statement/keyword.
let fullName = printFullName('Chase', 'Maze', 'Haste');
console.log(fullName); //undefined

function returnFullName(firstName, middleInitial, lastName){
	return firstName + ' ' + middleInitial + ' ' + lastName;
};

fullName = returnFullName("Juan", "Dela", "Cruz");
// printFullName(); return undefined because the function does not have return statement
//returnFUllName(); return a string as a value because it has a return statement. 
console.log(fullName);

// Functions which have a return statement are able to return value and it can be saved in a variable.
console.log(fullName + ' is a great name.');

function returnPhilippineAddress(city){
	return city + ", Philippines";
};

// return - return values from a  function which we can save in a variable.
let myFullAddress = returnPhilippineAddress("Pampanga");
console.log(myFullAddress);

// returns true if number is divisible by 4, else returns false
function checkDivisibilityBy4(number){
	let remainder = number % 4;
	let isDivisibleBy4 = remainder === 0;

	// returns either true or false
	// not only can you return raw values/ data, you can also return a value.
	return isDivisibleBy4;
	// return keywork not only allows us to return a valuee but also ends the process of the function.
	console.log("I am run after a return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);
console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

function createPlayerInfo(username, level, job){
//	console.log('Username: ' + username + ' Level: ' + level + ' Job: ' + job);
	return('Username: ' + username + ' Level: ' + level + ' Job: ' + job);

};


let user1 = createPlayerInfo('white_night', 95, "Paladin")
console.log(user1);


function add(number1, number2){
	let addition = number1 + number2;
	return addition;
};

let addNumber = add(1,3);
console.log(addNumber);

function multiply(num1, num2){
	return num1 * num2;
};
let product = multiply(4,4);
console.log(product);


function circle(pi, rad){
	let area = pi * (rad**2);
	return area;
};
let circleArea = circle(3.1416, 5);
console.log(circleArea);




/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */




/*Assignment Answer*/

function percent(val, totalVal){
	let percentage = (val/totalVal) * 100;

	return percentage;
};

let wholePercentage = percent(75,100);
console.log('Your score is: ' + wholePercentage);

function passOrFail(){
let isPassed = wholePercentage;
return answer = isPassed >= 75;

};
let isPassingScore = passOrFail();
console.log('Is it a passing score? ' + isPassingScore);





